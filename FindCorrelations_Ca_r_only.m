clear
clc
close all

A = importdata('/Users/lglarsen/Google Drive/Everglades DPM/Water quality/DPM Monthly For JMP with residuals5_No2011orGap_ForMatlab (1).xlsx');
b = A.textdata;
c = A.data;
ResidlogTPdiff = c(:,435);
ResidlogTP = c(:,240);
SitelogTP = c(:,[24,38,52,66,80,93,107,121,135,149,163,177]);
logTPdiffResids = c(:,[436,438,439,440,442,443,444,445,446,447]);
logTPResids = c(:, [241,243,244,245,247,248,249,250,251,252]);
logTPdiff = c(:,319:330);
% TPprevious = c(:,180:191);
sites = {'C1', 'DB1', 'DB2', 'DB3', 'RS1', 'RS2', 'S1', 'UB1', 'UB2', 'UB3'};
allsites = {'C1','C2','DB1', 'DB2', 'DB3','L67A', 'RS1', 'RS2', 'S1', 'UB1', 'UB2', 'UB3'};
Residlabels = {'Resid_C1', 'Resid_DB1', 'Resid_DB2', 'Resid_DB3', 'Resid_RS1', 'Resid_RS2', 'Resid_S1', 'Resid_UB1', 'Resid_UB2', 'Resid_UB3'};
Residlabels2 = {'Resid_C1', 'Resid_C2', 'Resid_DB1', 'Resid_DB2', 'Resid_DB3', 'Resid_L67A', 'Resid_RS1', 'Resid_RS2', 'Resid_S1', 'Resid_UB1', 'Resid_UB2', 'Resid_UB3'};
Difflabels = {'C1_diff','C2_diff', 'DB1_diff', 'DB2_diff', 'DB3_diff','L67A_diff', 'RS1_diff', 'RS2_diff', 'S1_diff', 'UB1_diff', 'UB2_diff', 'UB3_diff'};
Previouslabels = {'C1_previous','C2_previous', 'DB1_previous', 'DB2_previous', 'DB3_previous','L67A_previous', 'RS1_previous', 'RS2_previous', 'S1_previous', 'UB1_previous', 'UB2_previous', 'UB3_previous'};
%year = c(:,2); %VERIFY
sampledsites = b(2:end,2);
inds_for_elimination = [1,0,2,3,4,0,5:10,1:10,1,0,2,3,4,0,5:10]; %Numbers matching ii should be eliminated in the vector of indices to map correlation to.New
map_correlation_to = [1:12, 1, 3:5, 7:12, 1:12]; %Column indices to map correlations to. New
adj_matrix = zeros(10,12); %initialize. New

for ii = 1:10;
    myrows = find(ismember(sampledsites, sites{ii}));
    mycols = find(ismember(allsites, sites{ii}));
    TPdiffInput = [ResidlogTPdiff(myrows), SitelogTP(myrows, setdiff(1:12, mycols)), logTPdiffResids(myrows, setdiff(1:10, ii)), logTPdiff(myrows, setdiff(1:12, mycols))];
    TPInput = [ResidlogTP(myrows), SitelogTP(myrows, setdiff(1:12, mycols)), logTPResids(myrows, setdiff(1:10, ii))]; %, TPprevious(myrows, mycols)];
    TotalDiffLabels = [allsites(setdiff(1:12, mycols)), Residlabels(setdiff(1:10,ii)), Difflabels(setdiff(1:12, mycols))];
    TotalLabels = [allsites(setdiff(1:12, mycols)), Residlabels(setdiff(1:10, ii))]; %, Previouslabels(mycols)];
    map_to_rd = map_correlation_to(inds_for_elimination~=ii); %Column indices to map correlations to, resid diff model. New
    map_to = map_correlation_to(inds_for_elimination(1:22)~=ii); %column indices to map correlations to, non diff model. New.

    rpTPdiff = NaN(3, size(TPdiffInput,2)-1);
    for jj = 1:size(TPdiffInput, 2)-1
        TPdiffIn = TPdiffInput(:, [1, jj+1]);
        TPdiffIn = TPdiffIn(~isnan(sum(TPdiffIn, 2)), :); %Remove rows with NaNs
        TPdiffIn = TPdiffIn-repmat(mean(TPdiffIn,1), size(TPdiffIn,1), 1);
        [r, p] = corrcoef(TPdiffIn);
        rpTPdiff(1,jj) = r(1,2);
%         rMonte = NaN(1,1000);
%         for kk = 1:1000
%             r = corrcoef([randsample(TPdiffIn(:,1), size(TPdiffIn,1)), randsample(TPdiffIn(:,2), size(TPdiffIn,1))]);
%             rMonte(kk) = r(1,2);
%         end
%         pMonte = 1-numel(find(abs(rMonte)<=abs(rpTPdiff(1,jj))))./1000;
        rpTPdiff(2,jj) = p(1,2);
%         rpTPdiff(3,jj) = pMonte;
    end
%     sigs = find(rpTPdiff(3,:)<=0.05);
%     %figure, barh(rpTPdiff(1,sigs), 1);
%     %title([sites(ii), ' TPdiff'])
%     %set(gca, 'YTickLabel', TotalDiffLabels(sigs))
%     d1 = [TotalDiffLabels; num2cell(rpTPdiff)];
%     sigs2 = find(rpTPdiff(3,:)<=0.1);
%     sigs3 = find(ismember(sigs2, sigs));
%     T = zeros(numel(sigs)+1, numel(sigs2)+1);
%     for ll = 1:numel(sigs)
%         T(1+ll, sigs3(ll)) = 1;
%     end
%     TPdiffInput2 = TPdiffInput(:,[1,sigs2+1]);
%     TPdiffInput2 = TPdiffInput2(~isnan(sum(TPdiffInput2,2)),:);
%     readin = array2table([TPdiffInput2(:,2:end), TPdiffInput2(:,1)], 'VariableNames', [TotalDiffLabels(sigs2), 'ResidTPdiff']);
%     mdlDdb = stepwiselm(readin,  T, 'ResponseVar', 'ResidTPdiff','Upper', 'linear')
%     mdlDdb.ModelCriterion.AICc
%     mdlDdf = stepwiselm(readin, 'constant', 'ResponseVar', 'ResidTPdiff', 'Upper', 'linear')
%     mdlDdf.ModelCriterion.AICc
%     
%     rpSUVAdiff = NaN(3, size(SUVAdiffInput,2)-1);
%     for jj = 1:size(SUVAdiffInput, 2)-1
%         SUVAdiffIn = SUVAdiffInput(:, [1, jj+1]);
%         SUVAdiffIn = SUVAdiffIn(~isnan(sum(SUVAdiffIn, 2)), :); %Remove rows with NaNs
%         SUVAdiffIn = SUVAdiffIn-repmat(mean(SUVAdiffIn,1), size(SUVAdiffIn,1), 1);
%         [r, p] = corrcoef(SUVAdiffIn);
%         rpSUVAdiff(1,jj) = r(1,2);
%         rMonte = NaN(1,1000);
%         for kk = 1:1000
%             r = corrcoef([randsample(SUVAdiffIn(:,1), size(SUVAdiffIn,1)), randsample(SUVAdiffIn(:,2), size(SUVAdiffIn,1))]);
%             rMonte(kk) = r(1,2);
%         end
%         pMonte = 1-numel(find(abs(rMonte)<=abs(rpSUVAdiff(1,jj))))./1000;
%         rpSUVAdiff(2,jj) = p(1,2);
%         rpSUVAdiff(3,jj) = pMonte;
%     end
%     sigs = find(rpSUVAdiff(3,:)<=0.05);
%     figure, barh(rpSUVAdiff(1,sigs), 1);
%     title([sites(ii), ' SUVAdiff'])
%     set(gca, 'YTickLabel', TotalDiffLabels(sigs))
%     d2 = [TotalDiffLabels; num2cell(rpSUVAdiff)];
%         sigs2 = find(rpSUVAdiff(3,:)<=0.1);
%     sigs3 = find(ismember(sigs2, sigs));
%     T = zeros(numel(sigs)+1, numel(sigs2)+1);
%     for ll = 1:numel(sigs)
%         T(1+ll, sigs3(ll)) = 1;
%     end
%     SUVAdiffInput2 = SUVAdiffInput(:,[1,sigs2+1]);
%     SUVAdiffInput2 = SUVAdiffInput2(~isnan(sum(SUVAdiffInput2,2)),:);
%     readin = array2table([SUVAdiffInput2(:,2:end), SUVAdiffInput2(:,1)], 'VariableNames', [TotalDiffLabels(sigs2), 'SUVAdiff']);
%     mdlSdb = stepwiselm(readin,  T, 'ResponseVar', 'SUVAdiff','Upper', 'linear')
%     mdlSdb.ModelCriterion.AICc
%     mdlSdf = stepwiselm(readin, 'constant', 'ResponseVar', 'SUVAdiff', 'Upper', 'linear')
%     mdlSdf.ModelCriterion.AICc

    
    rpTP = NaN(3, size(TPInput,2)-1);
    for jj = 1:size(TPInput, 2)-1
        TPIn = TPInput(:, [1, jj+1]);
        TPIn = TPIn(~isnan(sum(TPIn, 2)), :); %Remove rows with NaNs
        TPIn = TPIn-repmat(mean(TPIn,1), size(TPIn,1), 1);
        [r, p] = corrcoef(TPIn);
        rpTP(1,jj) = r(1,2);
%         rMonte = NaN(1,1000);
%         for kk = 1:1000
%             r = corrcoef([randsample(TPIn(:,1), size(TPIn,1)), randsample(TPIn(:,2), size(TPIn,1))]);
%             rMonte(kk) = r(1,2);
%         end
%         pMonte = 1-numel(find(abs(rMonte)<=abs(rpTP(1,jj))))./1000;
        rpTP(2,jj) = p(1,2);
%         rpTP(3,jj) = pMonte;
    end
%     sigs = find(rpTP(3,:)<=0.05);
% %     figure, barh(rpTP(1,sigs), 1);
% %     title([sites(ii), ' TP'])
% %     set(gca, 'YTickLabel', TotalLabels(sigs))
%     d3 = [TotalLabels; num2cell(rpTP)];
%     sigs2 = find(rpTP(3,:)<=0.1);
%     sigs3 = find(ismember(sigs2, sigs));
%     T = zeros(numel(sigs)+1, numel(sigs2)+1);
%     for ll = 1:numel(sigs)
%         T(1+ll, sigs3(ll)) = 1;
%     end
%     TPInput2 = TPInput(:,[1,sigs2+1]);
%     TPInput2 = TPInput2(~isnan(sum(TPInput2,2)),:);
%     readin = array2table([TPInput2(:,2:end), TPInput2(:,1)], 'VariableNames', [TotalLabels(sigs2), 'TP']);
%     mdlDb = stepwiselm(readin,  T, 'ResponseVar', 'TP','Upper', 'linear')
%     mdlDb.ModelCriterion.AICc
%     mdlDf = stepwiselm(readin, 'constant', 'ResponseVar', 'TP', 'Upper', 'linear')
%     mdlDf.ModelCriterion.AICc
    for jj = 1:12 %Loop over sites that could be correlated. This whole loop is new.
        selectedcols_rd = find(map_to_rd==jj);
        selectedcols = find(map_to==jj); 
        subsetp = [rpTPdiff(2,selectedcols_rd), rpTP(2, selectedcols)]; %Just the p-values associated with this correlated site
        thisr = max([rpTPdiff(1,selectedcols_rd), rpTP(1, selectedcols)].*(subsetp<=0.05)); %Pick the maximum correlation from among the significant ones
        thisr = (thisr>0).*thisr; %If the correlation is negative, set it equal to zero
        if isempty(thisr), thisr = 0; end
        adj_matrix(ii,jj) = thisr; %Assign value to the adjacency matrix
    end
    
    
%     rpSUVA = NaN(3, size(SUVAInput,2)-1);
%     for jj = 1:size(SUVAInput, 2)-1
%         SUVAIn = SUVAInput(:, [1, jj+1]);
%         SUVAIn = SUVAIn(~isnan(sum(SUVAIn, 2)), :); %Remove rows with NaNs
%         SUVAIn = SUVAIn-repmat(mean(SUVAIn,1), size(SUVAIn,1), 1);
%         [r, p] = corrcoef(SUVAIn);
%         rpSUVA(1,jj) = r(1,2);
%         rMonte = NaN(1,1000);
%         for kk = 1:1000
%             r = corrcoef([randsample(SUVAIn(:,1), size(SUVAIn,1)), randsample(SUVAIn(:,2), size(SUVAIn,1))]);
%             rMonte(kk) = r(1,2);
%         end
%         pMonte = 1-numel(find(abs(rMonte)<=abs(rpSUVA(1,jj))))./1000;
%         rpSUVA(2,jj) = p(1,2);
%         rpSUVA(3,jj) = pMonte;
%     end
%     sigs = find(rpSUVA(3,:)<=0.05);
%     figure, barh(rpSUVA(1,sigs), 1);
%     title([sites(ii), ' SUVA'])
%     set(gca, 'YTickLabel', TotalLabels(sigs))
%     d4 = [TotalLabels; num2cell(rpSUVA)];
%     sigs2 = find(rpSUVA(3,:)<=0.1);
%     sigs3 = find(ismember(sigs2, sigs));
%     T = zeros(numel(sigs)+1, numel(sigs2)+1);
%     for ll = 1:numel(sigs)
%         T(1+ll, sigs3(ll)) = 1;
%     end
%     SUVAInput2 = SUVAInput(:,[1,sigs2+1]);
%     SUVAInput2 = SUVAInput2(~isnan(sum(SUVAInput2,2)),:);
%     readin = array2table([SUVAInput2(:,2:end), SUVAInput2(:,1)], 'VariableNames', [TotalLabels(sigs2), 'SUVA']);
%     mdlSb = stepwiselm(readin,  T, 'ResponseVar', 'SUVA','Upper', 'linear')
%     mdlSb.ModelCriterion.AICc
%     mdlSf = stepwiselm(readin, 'constant', 'ResponseVar', 'SUVA', 'Upper', 'linear')
%     mdlSf.ModelCriterion.AICc
    
end

adj_matrix = [adj_matrix(1,:); zeros(1,12); adj_matrix(2:4,:); zeros(1,12); adj_matrix(5:10,:)]; %Re-insert sites C2 and canal. New.
adj_matrix(2,:) = adj_matrix(:,2)';%Fill in row for C2. New.
adj_matrix(6,:) = adj_matrix(:,6)';%Fill in row for canal. New.
adj_matrix = max(triu(adj_matrix,1)+triu(adj_matrix,1)', tril(adj_matrix,-1)+tril(adj_matrix,-1)'); %Make the matrix undirected. Sometimes it won't have been symmetric before because you're always comparing the residual of the row variable to a column variable that may or may not be a residual. New.
save('/Users/lglarsen/Google Drive/Everglades DPM/Water quality/Results of WQ regression on residuals_final 2016/AdjCaNoFireOrFlood.mat', 'adj_matrix') %New
    